import runSequence from 'run-sequence';

export default function (cb) {

	runSequence('clean', 'build', cb);

};
