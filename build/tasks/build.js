import gulp from 'gulp';
import webpackStream from 'webpack-stream';

import config from '../config/config';
import webpackConfig from '../config/webpack.config';

export default function () {

	return gulp.src(config.source.root + config.source.mainFile)
		.pipe(webpackStream(webpackConfig))
		.pipe(gulp.dest(config.target.root));

};
