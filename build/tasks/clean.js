import del from 'del';

import config from '../config/config';

export default function () {

	return del([config.target.root]);

};
