import argv from '../util/argv';

export default {

	watch: argv.watch,
	debug: argv.debug,
	devtool: argv.debug ? 'source-map' : null,

	output: {
		filename: '[name].js'
	},

	module: {
		loaders: [{
			test: /\.js$/,
			loader: 'babel-loader'
		}]
	},

	resolve: {
		extensions: ['', '.js']
	}

};
