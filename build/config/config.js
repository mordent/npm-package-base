export default {

	build: {
		root: 'build/',
		tasks: {
			root: 'tasks/',
			files: '**/*.js'
		}
	},
	source : {
		root: 'src/',
		mainFile: 'main.js'
	},
	target: {
		root: 'dist/'
	}

};
