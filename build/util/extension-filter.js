import curry from 'curry';
import path from 'path';

const extensionFilter = curry((extension, filename) => {
	return (path.extname(filename) === extension);
});

export default extensionFilter;
