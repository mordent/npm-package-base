import minimist from 'minimist';

const argv = minimist(process.argv.slice(3), {});

export default argv;
