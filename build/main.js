import fs from 'fs';
import gulp from 'gulp';
import scriptFilter from './util/script-filter';

import config from './config/config';

export default function () {

	// synchronous, as tasks must be defined by end of script execution;
	// filtered, to remove any accidental or hidden files in the directory;
	// note that path is relative to process' current working directory
	const taskFilenames = fs.readdirSync(config.build.root + config.build.tasks.root).filter(scriptFilter);

	// load tasks
	taskFilenames.forEach((taskFilename) => {

		// remove extension ('.js')
		const taskName = taskFilename.slice(0, -3);
		// load task
		const task = require('./' + config.build.tasks.root + taskFilename).default;
		gulp.task(taskName, task);

	});

};
